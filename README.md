
# Resume App

Angular-based website designed to present my resume.

[**Find me here**](https://kamil-petk-resume-app.vercel.app)

## Functionality overview

* **Single Page Application:** Dynamic website structure with navigation between resume sections
* **Resume Sections:** Organized sections including "find me", "about me", "skills", "side projects" and "languages"
* **Menu Navigation:** Easily navigate between different sections of the page
* **Responsive Design**

<div>
  <p align="center">
    <img src="src/assets/svg/logo_angular_wht.svg" alt="angular-logo" width="110px" height="120px"/>
  </p>
</div>
