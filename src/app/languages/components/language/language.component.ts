import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'resume-app-language',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <h2 class="language">{{ language }}</h2>
    <p class="level">{{ level }}</p>
  `,
  styleUrls: ['./language.component.scss'],
})
export class LanguageComponent {
  @Input({ required: true }) language!: string;
  @Input({ required: true }) level!: string;
}

