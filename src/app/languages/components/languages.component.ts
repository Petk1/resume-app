import { AfterViewInit, ChangeDetectionStrategy, Component } from '@angular/core';
import { SectionHeaderComponent } from 'src/app/section-header/components/section-header..component';
import { BaseSectionComponent } from 'src/app/shared/components/base-section/base-section.component';
import { LanguageComponent } from './language/language.component';

@Component({
  selector: 'resume-app-languages',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss'],
  imports: [
    SectionHeaderComponent,
    LanguageComponent,
  ]
})
export class LanguagesComponent extends BaseSectionComponent implements AfterViewInit {
  ngAfterViewInit(): void {
    this.setDisplay();
  }
}
