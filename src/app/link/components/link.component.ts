import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'resume-app-link',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="link" (click)="onClick()">
      <img
        [src]="iconPath"
        [alt]="name"
        class="icon"
      >
      {{ name }}
    </div>
  `,
  styleUrls: ['./link.component.scss'],
})
export class LinkComponent {
  @Input({ required: true }) name!: string;
  @Input({ required: true }) iconPath!: string;
  @Input({ required: true }) url!: string;

  onClick(): void {
    window.location.href = this.url;
  }
}
