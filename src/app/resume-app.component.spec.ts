import { TestBed } from '@angular/core/testing';
import { ResumeAppComponent } from './resume-app.component';

describe('ResumeAppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ResumeAppComponent],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(ResumeAppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have the 'resume-app' title`, () => {
    const fixture = TestBed.createComponent(ResumeAppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('resume-app');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(ResumeAppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('h1')?.textContent).toContain('Hello, resume-app');
  });
});
