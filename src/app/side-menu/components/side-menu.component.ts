import { Component, ChangeDetectionStrategy, HostListener } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { Section } from '../enums/section.enum';

@Component({
  selector: 'resume-app-side-menu',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './side-menu.component.html',
  styleUrl: './side-menu.component.scss',
  imports: [
    MatIconModule,
  ],
})
export class SideMenuComponent {
  sectionsToSelect = Section;
  innerWidth = window.innerWidth;

  @HostListener('window:resize')
  onResize() {
    this.innerWidth = window.innerWidth;
  }

  scrollTo(section: Section): void {
    const sectionElement = document.querySelector(section);
    sectionElement?.scrollIntoView();
    for(let time = 5; time <= 200; time += 5){
      setTimeout(() => sectionElement?.scrollIntoView(), time);
    }
  }
}
