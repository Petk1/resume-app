export enum Section {
  MAIN_HEADER = 'resume-app-main-header',
  FIND_ME = 'resume-app-find-me',
  ABOUT_ME = 'resume-app-about-me',
  SKILLS = 'resume-app-skills',
  SIDE_PROJECTS = 'resume-app-side-projects',
  LANGUAGES = 'resume-app-languages',
}
