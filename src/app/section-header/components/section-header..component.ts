import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'resume-app-section-header.',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<div [class.header]="startAnimation">{{ header }}</div>`,
  styleUrls: ['./section-header.component.scss'],
})
export class SectionHeaderComponent {
  @Input({ required: true }) header!: string;
  @Input({ required: true }) startAnimation!: boolean;
}
