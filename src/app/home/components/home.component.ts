import { Component, ChangeDetectionStrategy, HostListener } from '@angular/core';
import { AboutMeComponent } from 'src/app/about-me/components/about-me.component';
import { FindMeComponent } from 'src/app/find-me/components/find-me.component';
import { LanguagesComponent } from 'src/app/languages/components/languages.component';
import { MainHeaderComponent } from 'src/app/main-header/components/main-header.component';
import { DividerComponent } from 'src/app/shared/components/divider/divider.component';
import { SideProjectsComponent } from 'src/app/side-projects/components/side-projects.component';
import { SkillsComponent } from 'src/app/skills/components/skills.component';

@Component({
  selector: 'resume-app-home',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
  imports: [
    MainHeaderComponent,
    DividerComponent,
    FindMeComponent,
    AboutMeComponent,
    SkillsComponent,
    SideProjectsComponent,
    LanguagesComponent,
  ],
})
export class HomeComponent {
  innerWidth = window.innerWidth;
  innerHeight = window.innerHeight;

  @HostListener('window:resize')
  onResize() {
    this.innerWidth = window.innerWidth;
    this.innerHeight = window.innerHeight;
  }
}
