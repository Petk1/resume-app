import { Component, ChangeDetectionStrategy, AfterViewInit} from '@angular/core';

import { SideProjectComponent } from './side-project/side-project.component';
import { SectionHeaderComponent } from 'src/app/section-header/components/section-header..component';
import { BaseSectionComponent } from 'src/app/shared/components/base-section/base-section.component';

@Component({
  selector: 'resume-app-side-projects',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './side-projects.component.html',
  styles: `
    :host {
      display: block;
      padding-top: 55px;
    }

    .component-placeholder {
      height: 4000px;
    }

    resume-app-side-project {
      margin-bottom: 100px;
    }
  `,
  imports: [
    SectionHeaderComponent,
    SideProjectComponent,
  ]
})
export class SideProjectsComponent extends BaseSectionComponent implements AfterViewInit {
  ngAfterViewInit(): void {
    this.setDisplay();
  }
}
