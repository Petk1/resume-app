import { AfterViewInit, ChangeDetectionStrategy, Component, HostListener, Input } from '@angular/core';
import { BaseSectionComponent } from 'src/app/shared/components/base-section/base-section.component';

@Component({
  selector: 'resume-app-side-project',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './side-project.component.html',
  styleUrls: ['./side-project.component.scss'],
})
export class SideProjectComponent extends BaseSectionComponent implements AfterViewInit {
  @Input({ required: true }) iconPath!: string;
  @Input({ required: true }) name!: string;
  @Input({ required: true }) description!: string;
  @Input({ required: true }) demoUrl!: string;
  @Input({ required: true }) codeUrl!: string;

  ngAfterViewInit(): void {
    this.setDisplay();
  }

  navigateToDemo(): void {
    window.location.href = this.demoUrl;
  }

  navigateToSourceCode(): void {
    window.location.href = this.codeUrl;
  }
}
