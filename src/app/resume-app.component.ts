import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { HomeComponent } from './home/components/home.component';
import { SideMenuComponent } from './side-menu/components/side-menu.component';

@Component({
  selector: 'resume-app-root',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './resume-app.component.html',
  styleUrls: ['./resume-app.component.scss'],
  imports: [
    RouterOutlet,
    HomeComponent,
    SideMenuComponent,
  ],
})
export class ResumeAppComponent {
  title = 'resume-app';
}
