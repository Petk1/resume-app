import { Component, ChangeDetectionStrategy, HostListener } from '@angular/core';
import { LinkComponent } from 'src/app/link/components/link.component';
import { SectionHeaderComponent } from 'src/app/section-header/components/section-header..component';

@Component({
  selector: 'resume-app-find-me',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    @if(innerWidth < 768 || innerHeight < 600) {
      <resume-app-section-header [header]="'find me'" [startAnimation]="true"/>
    }
    <div class="links display-content">
      <resume-app-link
        name="LinkedIn"
        iconPath="assets/img/linkedin_logo.png"
        url="https://www.linkedin.com/in/kamil-petk/"
      />
      <resume-app-link
        name="Gitlab"
        iconPath="assets/img/gitlab_logo.png"
        url="https://gitlab.com/Petk1"
      />
    </div>
  `,
  styleUrl: './find-me.component.scss',
  imports: [
    SectionHeaderComponent,
    LinkComponent,
  ]
})
export class FindMeComponent {
  innerWidth = window.innerWidth;
  innerHeight = window.innerHeight;

  @HostListener('window:resize')
  onResize() {
    this.innerWidth = window.innerWidth;
    this.innerHeight = window.innerHeight;
  }
}
