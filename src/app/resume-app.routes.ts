import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('src/app/home/home.routes')
        .then(({ homeRoutes }) => homeRoutes),
  },
];
