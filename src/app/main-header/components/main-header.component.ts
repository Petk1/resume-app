import { Component, ChangeDetectionStrategy } from '@angular/core';
import { LinkComponent } from 'src/app/link/components/link.component';

@Component({
  selector: 'resume-app-main-header',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="header">
      <div class="short-description">
        <div>Kamil Petk</div>
        <div>Angular Developer</div>
        <div>Poland</div>
      </div>
    </div>
  `,
  styleUrls: ['./main-header.component.scss'],
  imports: [
    LinkComponent,
  ]
})
export class MainHeaderComponent {}
