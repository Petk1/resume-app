import { ChangeDetectionStrategy, Component, AfterViewInit } from '@angular/core';

import { SectionHeaderComponent } from 'src/app/section-header/components/section-header..component';
import { BaseSectionComponent } from 'src/app/shared/components/base-section/base-section.component';

@Component({
  selector: 'resume-app-about-me',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss'],
  imports: [
    SectionHeaderComponent,
  ]
})
export class AboutMeComponent extends BaseSectionComponent implements AfterViewInit {
  ngAfterViewInit(): void {
    this.setDisplay();
  }
}
