import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'resume-app-divider',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '',
  styles: `
    @use 'mixins/animated-change-bg-color';

    :host {
      display: block;
      width: 100%;
      height: 2px;
      margin: 65px 0 0;
      @include animated-change-bg-color.change-bg-color(
        border-box,
        (change-bg-color 15s cubic-bezier(.85,.15,.1,.84) infinite),
        400%
      ) {
        @keyframes change-bg-color {
          0% {
            background-position: 0 100%;
          }
          50% {
            background-position: 100% 0;
          }
          100% {
            background-position: 0 100%;
          }
        }
      }
    }
  `
})
export class DividerComponent {}
