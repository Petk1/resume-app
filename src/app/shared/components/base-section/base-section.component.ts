import { Component, ChangeDetectionStrategy, signal, inject, ElementRef, HostListener } from '@angular/core';

@Component({
  selector: 'resume-app-base-section',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: ``,
})
export class BaseSectionComponent {
  private elementRef = inject(ElementRef);

  display = signal(false);

  @HostListener('window:scroll')
  scroll(): void {
    if(this.display()) return;
    this.setDisplay();
  }

  setDisplay() {
    const rect = this.elementRef.nativeElement.getBoundingClientRect();
    this.display.set(rect.top + 60 < window.innerHeight);
  }
}
