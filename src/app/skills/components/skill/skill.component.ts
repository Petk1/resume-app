import { Component, ChangeDetectionStrategy, Input, AfterViewInit } from '@angular/core';
import { NgOptimizedImage } from '@angular/common';
import { BaseSectionComponent } from 'src/app/shared/components/base-section/base-section.component';

@Component({
  selector: 'resume-app-skill',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    @defer(when display()) {
      <div class="display-content skill-container">
        <img
          [ngSrc]="iconPath"
          [alt]="name + ' Icon'"
          class="icon"
          width="130"
          height="130"
          priority
        >
        <p class="name">{{ name }}</p>
      </div>
    }
    @placeholder(minimum 100ms){
      <div class="component-placeholder"></div>
    }
  `,
  styleUrls: ['./skill.component.scss'],
  imports: [
    NgOptimizedImage
  ]
})
export class SkillComponent extends BaseSectionComponent implements AfterViewInit {
  @Input({ required: true }) iconPath!: string;
  @Input({ required: true }) name!: string;

  ngAfterViewInit(): void {
    this.setDisplay();
  }
}
