import { Component, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';

import { SkillComponent } from './skill/skill.component';
import { SectionHeaderComponent } from 'src/app/section-header/components/section-header..component';
import { BaseSectionComponent } from 'src/app/shared/components/base-section/base-section.component';

@Component({
  selector: 'resume-app-skills',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss'],
  imports: [
    SectionHeaderComponent,
    SkillComponent,
  ]
})
export class SkillsComponent extends BaseSectionComponent implements AfterViewInit {
  ngAfterViewInit(): void {
    this.setDisplay();
  }
}
