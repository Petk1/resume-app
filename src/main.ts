import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/resume-app.config';
import { ResumeAppComponent } from './app/resume-app.component';

bootstrapApplication(ResumeAppComponent, appConfig)
  .catch((err) => console.error(err));
